.PHONY = default iso

default:
	rm -rv packer 2>/dev/null || true
	ansible-playbook playbook.yml --ask-vault-pass

iso:
	rm -rv packer 2>/dev/null || true
	ansible-playbook playbook.yml --ask-vault-pass --extra-vars "repack_iso=repack"
